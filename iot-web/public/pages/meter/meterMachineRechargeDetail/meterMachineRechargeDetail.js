/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            meterMachineDetailInfo: {
                details: [],
                total: 0,
                records: 1,
                moreCondition: false,
                machineId: '',
                conditions: {
                    machineNameLike: "",
                    addressLike: "",
                    state: "",
                    roomNameLike: "",
                    communityId: "",
                    detailType: "1001",
                    queryStartTime:'',
                    queryEndTime:''
                }
            }
        },
        _initMethod: function () {
            $that._listMeterMachineDetails(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.initDateTime('queryStartTime',function(_value){
                $that.meterMachineDetailInfo.conditions.queryStartTime = _value;
            });
            vc.initDateTime('queryEndTime',function(_value){
                $that.meterMachineDetailInfo.conditions.queryEndTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('pagination', 'page_event', function (_currentPage) {
                $that._listMeterMachineDetails(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listMeterMachineDetails: function (_page, _rows) {

                $that.meterMachineDetailInfo.conditions.page = _page;
                $that.meterMachineDetailInfo.conditions.row = _rows;
                $that.meterMachineDetailInfo.conditions.communityId = vc.getCurrentCommunity().communityId;
                let param = {
                    params: $that.meterMachineDetailInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/meterMachineCharge.listMeterMachineCharge',
                    param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        $that.meterMachineDetailInfo.total = _json.total;
                        $that.meterMachineDetailInfo.records = _json.records;
                        $that.meterMachineDetailInfo.details = _json.data;
                        vc.emit('pagination', 'init', {
                            total: $that.meterMachineDetailInfo.records,
                            currentPage: _page,
                            dataCount:_json.total
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
         
            _queryMeterMachineDetailMethod: function () {
                $that._listMeterMachineDetails(DEFAULT_PAGE, DEFAULT_ROWS);
            },
            _resetMeterMachineDetailMethod: function () {
                $that.meterMachineDetailInfo.conditions = {
                    machineNameLike: "",
                    addressLike: "",
                    state: "",
                    roomNameLike: "",
                    communityId: "",
                    detailType: "1001",
                    queryStartTime:'',
                    queryEndTime:''
                }
                $that._listMeterMachineDetails(DEFAULT_PAGE, DEFAULT_ROWS);
            },
            _moreCondition: function () {
                if ($that.meterMachineDetailInfo.moreCondition) {
                    $that.meterMachineDetailInfo.moreCondition = false;
                } else {
                    $that.meterMachineDetailInfo.moreCondition = true;
                }
            }
        }
    });
})(window.vc);
