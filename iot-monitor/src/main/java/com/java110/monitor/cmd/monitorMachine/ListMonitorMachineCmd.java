/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.monitor.cmd.monitorMachine;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.utils.Assert;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.dto.monitor.MonitorMachineAttrsDto;
import com.java110.dto.monitor.MonitorMachineDto;
import com.java110.intf.monitor.IMonitorMachineAttrsV1InnerServiceSMO;
import com.java110.intf.monitor.IMonitorMachineV1InnerServiceSMO;
import com.java110.monitor.factory.IMonitorCore;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;


/**
 * 类表述：查询
 * 服务编码：monitorMachine.listMonitorMachine
 * 请求路劲：/app/monitorMachine.ListMonitorMachine
 * add by 吴学文 at 2023-10-18 10:33:51 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */
@Java110Cmd(serviceCode = "monitorMachine.listMonitorMachine")
public class ListMonitorMachineCmd extends Cmd {

    private static Logger logger = LoggerFactory.getLogger(ListMonitorMachineCmd.class);
    @Autowired
    private IMonitorMachineV1InnerServiceSMO monitorMachineV1InnerServiceSMOImpl;

    @Autowired
    private IMonitorMachineAttrsV1InnerServiceSMO monitorMachineAttrsV1InnerServiceSMOImpl;

    @Autowired
    private IMonitorCore monitorCoreImpl;

    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) {
        super.validatePageInfo(reqJson);
        Assert.hasKeyAndValue(reqJson, "communityId", "请求报文中未包含communityId");

    }

    @Override
    public void doCmd(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) throws CmdException {

        MonitorMachineDto monitorMachineDto = BeanConvertUtil.covertBean(reqJson, MonitorMachineDto.class);

        int count = monitorMachineV1InnerServiceSMOImpl.queryMonitorMachinesCount(monitorMachineDto);

        List<MonitorMachineDto> monitorMachineDtos = null;

        if (count > 0) {
            monitorMachineDtos = monitorMachineV1InnerServiceSMOImpl.queryMonitorMachineList(monitorMachineDto);
            refreshAttr(monitorMachineDtos);
            // todo  查询设备是否在线
            queryMachineState(monitorMachineDtos);
        } else {
            monitorMachineDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reqJson.getInteger("row")), count, monitorMachineDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        cmdDataFlowContext.setResponseEntity(responseEntity);
    }

    private void refreshAttr(List<MonitorMachineDto> monitorMachineDtos) {
        if (CollectionUtils.isEmpty(monitorMachineDtos)) {
            return;
        }
        for (MonitorMachineDto monitorMachineDto : monitorMachineDtos) {
            MonitorMachineAttrsDto monitorMachineAttrsDto = new MonitorMachineAttrsDto();
            monitorMachineAttrsDto.setMachineId(monitorMachineDto.getMachineId());
            monitorMachineAttrsDto.setCommunityId(monitorMachineDto.getCommunityId());
            List<MonitorMachineAttrsDto> monitorMachineAttrsDtos = monitorMachineAttrsV1InnerServiceSMOImpl.queryMonitorMachineAttrss(monitorMachineAttrsDto);
            monitorMachineDto.setMonitorMachineAttrsList(monitorMachineAttrsDtos);
        }
    }

    private void queryMachineState(List<MonitorMachineDto> monitorMachineDtos) {
        if (monitorMachineDtos == null || monitorMachineDtos.size() < 1 || monitorMachineDtos.size() > 10) {
            return;
        }

        monitorCoreImpl.queryMonitorMachineState(monitorMachineDtos);
    }
}
