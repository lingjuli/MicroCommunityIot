package com.java110.dto.storeStaff;

import com.java110.bean.dto.PageDto;
import com.java110.dto.store.StoreDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 员工表数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class StoreStaffDto extends StoreDto implements Serializable {

    public static final String ADMIN_FLAG_Y = "Y";
    public static final String ADMIN_FLAG_N = "N";

    private String staffName;
    private String tel;
    private String storeId;
    private String adminFlag;
    private String storeStaffId;
    private String staffId;


    private Date createTime;

    private String statusCd = "0";


    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAdminFlag() {
        return adminFlag;
    }

    public void setAdminFlag(String adminFlag) {
        this.adminFlag = adminFlag;
    }

    public String getStoreStaffId() {
        return storeStaffId;
    }

    public void setStoreStaffId(String storeStaffId) {
        this.storeStaffId = storeStaffId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
